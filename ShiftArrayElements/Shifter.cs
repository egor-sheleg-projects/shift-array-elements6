﻿namespace ShiftArrayElements
{
    public static class Shifter
    {
        /// <summary>
        /// Shifts elements in a <see cref="source"/> array using <see cref="iterations"/> array for getting directions and iterations (see README.md for detailed instructions).
        /// </summary>
        /// <param name="source">A source array.</param>
        /// <param name="iterations">An array with iterations.</param>
        /// <exception cref="ArgumentNullException">source array is null.</exception>
        /// <exception cref="ArgumentNullException">iterations array is null.</exception>
        public static void Shift(int[] source, int[] iterations)
        {
            for (int i = 0; i < iterations.Length; i++)
            {
                if (iterations[i] == 0)
                {
                    continue;
                }

                if (i % 2 > 0)
                {
                    for (int k = 0; k < iterations[i]; k++)
                    {
                        int t = source[^1];
                        Array.Copy(source, 0, source, 1, source.Length - 1);
                        source[0] = t;
                    }
                }
                else if (i % 2 == 0 || i == 0)
                {
                    for (int k = 0; k < iterations[i]; k++)
                    {
                        int t = source[0];
                        Array.Copy(source, 1, source, 0, source.Length - 1);
                        source[^1] = t;
                    }
                }
            }
        }
    }
}
